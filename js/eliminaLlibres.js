/**
 * Created by dtorralbo on 04/09/2017.
 */

var eliminaLlibres = {

    /*
     Inicialitza les següents funcions
     */
    init:function(){
        this.initSelectors();
        this.enviarDB();
    },

    /*
     Inicialitza els selectors necessaris
     */
    initSelectors: function(){
        this.selectors = {};
        this.selectors.taula = $("#resultats tbody");
        this.selectors.modalEliminar = $('#confirmacioEliminar');
        this.selectors.botoEliminar = $('#elimConfirm');
    },

    /*
     Envia el llibre a la base de dades
     */
    enviarDB: function () {

        this.selectors.taula.on('click', '.btn', (function(ev) {

            this.selectors.modalEliminar.modal({
                keyboard: false
            })

            this.selectors.botoEliminar.on('click', (function() {
                $('#fila_'+ev.target.id).fadeOut("slow");
                this.selectors.modalEliminar.modal('toggle');

                $.ajax({
                    type: 'POST',
                    url: 'informacio.php',
                    data: {
                        delete: true,
                        id_elim: ev.target.id
                    },
                    dataType: 'json'
                });
            }.bind(this)))


        }.bind(this)));
    }
};

eliminaLlibres.init();