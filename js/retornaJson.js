/**
 * Created by dtorralbo on 04/09/2017.
 */

var populateTable = {

    /*
    Inicialitza les següents funcions
     */
    init:function(){
        this.template = _.template($("script.table-data").html());
        this.initSelectors();
        this.attachEventValue();
        this.dadesPlantilla();
    },

    /*
    Inicialitza els selectors necessaris
     */
    initSelectors: function(){
        this.selectors = {};
        this.selectors.tableBody= $("#resultats tbody");
        this.selectors.filtreTitol= $("#filtre_titol");
        this.selectors.seleccio = $('#tipus');
        this.selectors.guardar = $("#btn_guardaInfo");
    },

    /*
    Envia el valor de l'input per a consultar les dades
     */
    attachEventValue: function () {
        this.selectors.filtreTitol.on('input', (function(ev) {
            var filtre = ev.target.value;
            var tipus = this.selectors.seleccio.val();
            this.dadesPlantilla(filtre, tipus);
        }).bind(this));
    },

    /*
    Mitjançant un filtre, cerca els llibres pertinents
     */
    dadesPlantilla: function (filtre, tipus) {

        $.ajax({
            type: 'POST',
            url: 'informacio.php',
            data: {
                filtre: filtre,
                tipus: tipus
            },
            dataType: 'json',
            success: (function (data) {

                if(data.length) {
                    this.generarTemplate(data);
                } else {
                    this.selectors.tableBody.empty();
                    console.error("No s'han trobat dades.");
                }
            }.bind(this))
        });

    },

    /*
    Genera la template a partir de les dades obtingudes
     */
    generarTemplate: function(data){
    var templateData = {};
    templateData.listItems = [];

    this.selectors.tableBody.empty();

    //Per a cada objecte dins l'array fem un push
    $.each(data, function (index, value) {
        templateData.listItems.push({
            id: value.id,
            nom_llibre: value.nom_llibre,
            nom_autor: value.nom_autor,
            dni_autor: value.dni_autor,
            nom_editorial: value.nom_editorial,
            telefon_editorial: value.telefon_editorial,
            enviar: false
        });
    });

    this.selectors.tableBody.append(this.template(templateData));
    }
};

populateTable.init();