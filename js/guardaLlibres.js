/**
 * Created by dtorralbo on 04/09/2017.
 */

var guardaLlibres = {

    /*
     Inicialitza les següents funcions
     */
    init:function(){
        this.initSelectors();
        this.desplegarFormulari();
        this.enviarDB();
    },

    /*
     Inicialitza els selectors necessaris
     */
    initSelectors: function(){
        this.selectors = {};
        this.selectors.nouLlibre= $("#btn_nouLlibre");
        this.selectors.mostrarInsertar = $("#div_insertar");
        this.selectors.guardar = $("#btn_guardaInfo");
        this.selectors.cancelar = $("#btn_cancelaInfo");
        this.selectors.alerta = $("#alerta_nouLlibre");
        this.selectors.alertaError = $("#alerta_errorLlibre");
        this.selectors.form = $("#form_nouLlibre");
    },

    /*
     Desplega el formulari al pulsar el botó
     */
    desplegarFormulari: function () {
        this.selectors.nouLlibre.on('click', (function() {
            this.selectors.mostrarInsertar.slideToggle("fast");
        }).bind(this));

        this.selectors.cancelar.on('click', (function() {
            this.selectors.mostrarInsertar.slideToggle("fast");
        }).bind(this));
    },

    /*
     Envia el llibre a la base de dades
     */
    enviarDB: function () {

        this.selectors.guardar.on('click', (function() {

            //fem un array de tots els valors dels inputs entrats al formulari
            $.ajax({
                url: "informacio.php",
                type: "POST",
                dataType: "json",
                data:this.selectors.form.serialize()
            }).done((function(data) {
                $.each(data, (function (index, value) {
                    this.mostremAlertes(value);
                    this.carregaRetorn();
                    //reinicialitzem els valors del formulari
                    this.selectors.form.trigger("reset");
                }).bind(this));
            }).bind(this));

        }).bind(this));

    },

    /*
     Carrega l'arxiu que retorna els llibres en JSON
     */
    carregaRetorn: function () {
        $.getScript("/js/retornaJson.js")
            .done(function() {
                console.log('S\'han recarregat els llibres!');
            })
            .fail(function() {
                console.log('Hi ha hagut un problema');
            });
    },

    /*
     Mostra les alertes pertinents
     */
    mostremAlertes: function(value) {
        if(!value.error){
            this.selectors.mostrarInsertar.slideToggle("fast");
            this.selectors.alerta.fadeIn(1000);
            this.selectors.alerta.fadeOut(5000);
        } else {
            this.selectors.mostrarInsertar.slideToggle("fast");
            this.selectors.alertaError.fadeIn(1000);
            this.selectors.alertaError.fadeOut(5000);
        }
    }
};

guardaLlibres.init();