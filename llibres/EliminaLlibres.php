<?php
/**
 * Created by PhpStorm.
 * User: dtorralbo
 * Date: 12/09/2017
 * Time: 16:06
 */

namespace Llibreria;

use PDO;

class EliminaLlibres
{
    protected $conexio;

    /**
     * InsertaLlibres constructor.
     */
    public function __construct()
    {
        $this->conexio = new ConexioDB();
    }

    public function elimina($id){
        try {
            $sql = "DELETE FROM llibres WHERE id = :id";
            $stmt = $this->conexio->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (PDOException $ex) {
            echo  $ex->getMessage();
        }
    }
}