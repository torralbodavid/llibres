<?php
/**
 * Created by PhpStorm.
 * User: dtorralbo
 * Date: 07/09/2017
 * Time: 12:02
 */

namespace Llibreria;


class InsertaLlibres
{

    protected $conexio;

    /**
     * InsertaLlibres constructor.
     */
    public function __construct()
    {
        $this->conexio = new ConexioDB();
    }


    /**
     * @param $data
     */
    //Inserta les dades introduïdes a la base de dades segons el formulari
    public function inserta($data){

        try {
            $insertaAutor = $this->conexio->prepare("INSERT INTO autors(nom, dni)
    VALUES(?, ?)");
            $insertaAutor->execute(array($data['entrar_autor'], $data['entrar_dni']));

            $insertaEditorial = $this->conexio->prepare("INSERT INTO editorials(nom, telefon)
    VALUES(?, ?)");
            $insertaEditorial->execute(array($data['entrar_editorial'], $data['entrar_tel_editorial']));

            $llibresDetall = $this->conexio->prepare("INSERT INTO llibres_detall(nom)
    VALUES(?)");
            $llibresDetall->execute(array($data['entrar_titol']));

            $this->_entraLlibre($data['entrar_dni'], $data['entrar_tel_editorial'], $data['entrar_titol']);
        }catch (PDOException $e){
            return $this->_retornaResultat(true);
        }

    }

    /**
     * @param $dni
     * @param $telefon
     * @param $titol
     */
    //Entrem el llibre segons els paràmetres de les altres taules
    private function _entraLlibre($dni, $telefon, $titol){
        try {
            $idAutor = $this->conexio->prepare("SELECT id FROM autors WHERE dni = '" . $dni . "'");
            $idAutor->execute();

            $idEditorial = $this->conexio->prepare("SELECT id FROM editorials WHERE telefon = '" . $telefon . "'");
            $idEditorial->execute();

            $idLlibre = $this->conexio->prepare("SELECT id FROM llibres_detall WHERE nom = '" . $titol . "'");
            $idLlibre->execute();

            $result_idAutor = $idAutor->fetch();
            $result_idEditorial = $idEditorial->fetch();
            $result_idLlibre = $idLlibre->fetch();

            $insertaLlibre = $this->conexio->prepare("INSERT INTO llibres(id_llibre, id_editorial, id_autor)
    VALUES(?, ?, ?)");
            $insertaLlibre->execute(array($result_idLlibre['id'], $result_idEditorial['id'], $result_idAutor['id']));

            return $this->_retornaResultat(false);
        } catch (PDOException $e){
            return $this->_retornaResultat(true);
        }
    }


    /**
     * @param $status
     */
    //Retorna un JSON amb un paràmetre booleà depenent de si hi ha hagu error o no en el procés d'insertació de dades.
    private function _retornaResultat($status){

        $dades = array();
            $dades[] = array(
                'error' => $status);

        echo json_encode($dades);
    }
}