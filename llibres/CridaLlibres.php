<?php namespace Llibreria;

/**
 * Created by PhpStorm.
 * User: dtorralbo
 * Date: 06/09/2017
 * Time: 9:00
 */

use PDO as database;

class CridaLlibres
{
    protected $conexio;

    function __construct() {
        $this->conexio = new ConexioDB();
    }


    //Inserta noves dades

    /**
     * @param null $nom
     * @param int $tipus
     */
    public function cerca($nom=null, $tipus=1){

        $resultat = $this->conexio->prepare("SELECT llibres_detall.id as id_llibre, 
            llibres_detall.nom as nom_llibre, 
            editorials.nom as nom_editorial, 
            editorials.telefon as telefon_editorial, 
            autors.nom as nom_autor, 
            autors.dni as dni_autor FROM llibres 
            INNER JOIN llibres_detall ON llibres.id_llibre = llibres_detall.id 
            INNER JOIN editorials ON llibres.id_editorial = editorials.id
            INNER JOIN autors ON llibres.id_autor = autors.id WHERE ".$this->_comprovaTipus($tipus)." LIKE '%$nom%';");
        $resultat->execute();

        echo $this->_retornaLlibres($resultat);
    }

    /**
     * @param int $tipus
     * @return string
     */

    //Comprova el tipus de filtre que arriba per el formulari
    private function _comprovaTipus($tipus=1){
        $resposta = "llibres_detall.nom";

            switch ($tipus){
                case 1: $resposta = "llibres_detall.nom";
                    break;
                case 2: $resposta = "autors.nom";
                    break;
                case 3: $resposta = "autors.dni";
                    break;
                case 4: $resposta = "editorials.nom";
                    break;
                case 5: $resposta = "editorials.telefon";
                    break;
            }

        return $resposta;
    }

    /**
     * @param $resultat
     * @return string
     */
    private function _retornaLlibres($resultat){
        $dades = array();
        while($row = $resultat->fetch(database::FETCH_ASSOC)){
            $dades[] = array(
                'id' => $row['id_llibre'],
                'nom_llibre' => $row['nom_llibre'],
                'nom_editorial' => $row['nom_editorial'],
                'telefon_editorial' => $row['telefon_editorial'],
                'nom_autor' => $row['nom_autor'],
                'dni_autor' => $row['dni_autor']);
        }

        return json_encode($dades);
    }
}