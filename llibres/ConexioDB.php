<?php namespace Llibreria;

use PDO;
/**
 * Created by PhpStorm.
 * User: dtorralbo
 * Date: 06/09/2017
 * Time: 9:09
 */
class ConexioDB extends PDO
{
    public function __construct()
    {
        $usuari = getenv("DB_USER");
        $contrasenya = getenv("DB_PASS");

        try {
            parent::__construct('mysql:host=localhost;dbname=llibres', $usuari, $contrasenya);
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
}