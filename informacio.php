<?php
/**
 * Created by PhpStorm.
 * User: dtorralbo
 * Date: 06/09/2017
 * Time: 9:06
 */

require "vendor/autoload.php";
use Llibreria\CridaLlibres as crida;
use Llibreria\InsertaLlibres as inserta;
use Llibreria\EliminaLlibres as elimina;

//Cridar
$filtre = $_POST['filtre'];
$tipus = $_POST['tipus'];

//Enviar
$data = $_POST;

if($_POST['delete']){
    $eliminar = new elimina();
    $eliminar->elimina($_POST['id_elim']);
}

if(!$_POST['enviar']) {
    $llibres = new crida();
    $llibres->cerca($filtre, $tipus);
} else {
    $llibres = new inserta();
    $llibres->inserta($data);
}